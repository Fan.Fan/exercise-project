# Copyright (c) 2021 Helmholtz-Zentrum Dresden - Rossendorf e.V.
#
# SPDX-License-Identifier: MIT

cmake_minimum_required(VERSION 3.14)
project (helloWorld)
include_directories(${CMAKE_SOURCE_DIR})
add_subdirectory(src bin)

# GoogleTest requires at least C++14
set(CMAKE_CXX_STANDARD 14)

# Setup GoogleTest v1.13.0 and build tests
include(FetchContent)
FetchContent_Declare(
  googletest
  URL https://github.com/google/googletest/archive/b796f7d44681514f58a683a3a71ff17c94edb0c1.zip
)

# For Windows: Prevent overriding the parent project's compiler/linker settings
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(googletest)

enable_testing()

add_compile_options("--coverage")
add_link_options("--coverage")

add_executable(
  hello_test
  src/helloworld.cpp
  tests/tests.cpp
)
target_link_libraries(
  hello_test
  gtest_main
)

include(GoogleTest)
gtest_discover_tests(hello_test)
